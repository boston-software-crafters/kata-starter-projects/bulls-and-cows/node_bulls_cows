const { runBullsAndCowsPlayer } = require("./spoiler_alert/server.js")
const { Player } = require("./player.js")

// Run the Player against Bulls & Cows in 1,000 games
runBullsAndCowsPlayer(Player, 1000);