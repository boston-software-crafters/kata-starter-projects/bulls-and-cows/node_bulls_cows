// This Example Player guesses completely at random
// It takes over 10,000+ tries to win, on average...
// That is NOT very good!
// (Aren't there less than 10k possible codes?)

class Player {

  // Player can initialize state here
  constructor() {
    this.myState = {}
  }

  // Make a guess, based on feedback so far
  guess() {

    // Generate 4 digits, 0 - 9, Completely at random 
    let digits = []
    for (let i=0; i<4; i++) {
      digits.push(Math.floor(Math.random() * 10))
    }

    // Return the four digits as a string, e.g. "1234"
    return digits.join("")
  }

  // Recieve feedback for incorrect guesses
  get_feedback(guess, bulls, cows) {
    // FIXME: Do something with this information?
  }

}

module.exports = { Player }