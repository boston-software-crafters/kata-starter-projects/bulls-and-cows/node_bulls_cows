tests:
	./node_modules/mocha/bin/mocha.js --recursive
test: tests


tests-full:
	./node_modules/mocha/bin/mocha.js --recursive --full-trace true
test-full: tests-full

tests-watch:
	./node_modules/mocha/bin/mocha.js --recursive --watch 
test-watch: tests-watch

run:
	node index.js

