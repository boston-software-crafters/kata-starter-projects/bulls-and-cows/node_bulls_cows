const format = (number) => number.toLocaleString("en-US")

const DEFAULT_TARGET_GAMES = 10000

const runBullsAndCowsPlayer = (playerClass, target_games) => {
  if (!target_games) {
    target_games = DEFAULT_TARGET_GAMES
  }
  console.log(`Playing ${format(target_games)} games...`);
  let player;
  let total_guesses = 0;
  let total_games = 0;
  let min_guesses = null;
  let max_guesses = null;

  while (total_games < target_games) {
    player = new playerClass();
    let bulls = null;
    let cows = null;
    let guesses = 0;
    let secret_code = generateCode();
    while (bulls < 4) {
      let guess = player.guess();
      if (!guess) {
        console.error(`Disqualified: Got a falsey guess: ${guess}`)
        process.exit(1)
      }
      if (guess.length != 4) {
        console.error(`Disqualified: Got a Guess of length ${guess.length}: ${guess}`) 
        process.exit(1)
      }
      [ bulls, cows ] = scoreCode(secret_code, guess);
      player.get_feedback(guess, bulls, cows)
      guesses += 1
    }

    total_guesses += guesses;
    total_games += 1;

    if (min_guesses === null || guesses < min_guesses) {
      min_guesses = guesses
    }
    if (max_guesses === null || guesses > max_guesses) {
      max_guesses = guesses
    }

    let percent_done = Math.round((total_games / target_games) * 100)
    process.stdout.write(`   ${format(percent_done)}%  \r`);
  }
  let average_guesses = Math.round(total_guesses / total_games, 2);
  console.log(`Won in an average of ${format(average_guesses)} guesses per game!`);
  console.log();
  console.log(`   Minimum Guesses: ${format(min_guesses)}`);
  console.log(`   Maximum Guesses: ${format(max_guesses)}`);
  console.log(`       Total Games: ${format(total_games)}`);
  console.log(`     Total Guesses: ${format(total_guesses)}`);
  console.log();
}


const scoreCode = (code, guess) => {
  let codeArray = [...code]
  let misplaced = 0
  let correct = 0
  for (let i=0; i<4; i++) {
    let guessLetter = guess[i]
    let hasLetterAt = codeArray.findIndex( x => x === guessLetter)
    if (guessLetter == code[i]) {
      correct += 1
    } else if (hasLetterAt !== -1) {
      misplaced += 1
      // Remove from the array
      codeArray.splice(hasLetterAt, 1)
    }
  }
  return [correct, misplaced]
}

const generateCode = () => {
  let digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  let code = []
  digits = shuffleKnuth(digits)
  return digits.slice(0, 4).join("")
}

const sum = (...x) => {
    var sum = 0
    for (thisX of x) {
        sum += thisX
    }
    return sum
}

const shuffleKnuth = (array) => {
  // Fisher-Yates (aka Knuth) Shuffle.

  // Iterate backwards through the array
  let currentIndex = array.length, randomIndex;

  // While there remain elements to shuffle:
  while (currentIndex != 0) {

    // Pick a "remaining" element (up to currentindex)
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

const shuffleDurstenfeld = (array) => {
    // ES6 optimized Durstenfeld shuffle
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

module.exports = { sum, generateCode, scoreCode, runBullsAndCowsPlayer }
