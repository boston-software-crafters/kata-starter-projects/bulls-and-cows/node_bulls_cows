const { sum, generateCode, scoreCode } = require("../../spoiler_alert/server.js")

const assert = require('assert');

const repeatedly = 100

describe('main', function () {
  describe('sum()', function () {
    it('sums up (1, 2, 3) to 6', function () {
        assert(sum(1, 2, 3) === 6)
    });
  });
  describe('generateCode', () => {
    it(`Generates codes with four unique digits (x${repeatedly})`, () => {
      for (let i=0; i<repeatedly; i++) {
        let code = generateCode()
        assert(code.length === 4, `Codes must have four digits, got: ${code}`)
        let set = new Set()
        for (let letter of code) {
           assert(set.has(letter) === false, `Digits must be unique, got: ${code}`)
           set.add(letter)
        }
      }
    })
  });

  var tests = [
    {args: ['0000', '1111'], expected: [0, 0]},
    {args: ['1111', '1111'], expected: [4, 0]},
    {args: ['0000', '1101'], expected: [1, 0]},
    {args: ['0022', '2111'], expected: [0, 1]},
    {args: ['1234', '4321'], expected: [0, 4]},
    {args: ['2222', '4321'], expected: [1, 0]},
    {args: ['2225', '4322'], expected: [1, 1]},
  ];

  describe('scoreCode', () => {
    tests.forEach(function(test) {
      it(`${test.args[0]} guessed as ${test.args[1]} results in ${test.expected[0]} bulls, ${test.expected[1]} cows`, function() {
        assert.deepStrictEqual(scoreCode(...test.args), test.expected);
      });
    });
  });
});
